﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using TestWeb.App_Code;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace TestWeb.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Message = "Product Navigation";
			
			return View();
		}

		public JsonResult GetRootNode()
		{
			JsonResult r = new JsonResult();
			r.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			r.Data = GetRoot();
			return r;
		}		
		
		public JsonResult GetNodeChildren(string type = "", string args = "")
		{
			JsonResult r = new JsonResult();
			r.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

			switch (type)
			{
				case "root":
					r.Data = GetRoot();
					break;
				case "store":
					r.Data = GetCategories(args);
					break;
				case "category":
					r.Data = GetCategories(args);
					break;
				default:
					break;
			}

			return r;
		}

		private jsTreeNode GetRoot()
		{
			jsTreeNode rootNode = new jsTreeNode();
			rootNode.data = "Newegg.com";
			rootNode.children = new List<jsTreeNode>();
			rootNode.state = "open";

			foreach (NeweggAPI.Store store in NeweggAPI.API.GetAllStores())
			{
				jsTreeNode node = new jsTreeNode();

				node.data = store.Title;
				node.metadata = new { type = "store", args = "storeid:" + store.StoreID.ToString() };
				node.state = "closed";

				rootNode.children.Add(node);
			}

			return rootNode;
		}

		private List<jsTreeNode> GetCategories(string args)
		{
			List<jsTreeNode> nodes = new List<jsTreeNode>();
			
			ReadOnlyCollection<NeweggAPI.Category> categories;
			if(!args.Contains("nodeid"))
			{
				Match m = Regex.Match(args, "storeid\\:([\\d]+)");
				int storeID = Convert.ToInt32(m.Groups[1].Value);

				categories = NeweggAPI.API.GetMainCategories(storeID);
			}
			else
			{
				Match m = Regex.Match(args, "storeid\\:([\\d]+),parentcategoryid\\:([\\d]+),nodeid\\:([\\d]+)");
				int storeID = Convert.ToInt32(m.Groups[1].Value);
				int parentCategoryID = Convert.ToInt32(m.Groups[2].Value);
				int nodeID = Convert.ToInt32(m.Groups[3].Value);

				categories = NeweggAPI.API.GetSubCategories(storeID, parentCategoryID, nodeID);
			}

			foreach (NeweggAPI.Category category in categories)
			{
				jsTreeNode node = new jsTreeNode();
				node.data = category.Description;
				node.metadata = new { type = "category", args = "storeid:" + category.StoreID + ",parentcategoryid:" + category.CategoryID + ",nodeid:" + category.NodeId };
				node.state = "closed";

				//if (category.SubCategories.Count > 0)
				//{
				//    //node.children = GetSubCategories(category);
				//    node.state = "closed";
				//}

				nodes.Add(node);
			}

			return nodes;
		}

		private List<jsTreeNode> GetSubCategories(NeweggAPI.Category parent)
		{
			List<jsTreeNode> subCategories = new List<jsTreeNode>();
			foreach (NeweggAPI.Category subCategory in parent.SubCategories)
			{
				jsTreeNode node = new jsTreeNode();
				node.data = subCategory.Description;
				node.metadata = new { type = "subcategory", args = subCategory.CategoryID };

				subCategories.Add(node);
			}
			return subCategories;
		}
	}
}
