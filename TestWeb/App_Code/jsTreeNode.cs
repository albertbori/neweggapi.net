﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.App_Code
{
	[Serializable]
	public class jsTreeNode
	{
		/// <summary>
		/// This can be a string for the name, or a jsTreeData object to manipulate the <a> tag
		/// </summary>
		public object data { get; set; } 

		/// <summary>
		/// Attributes
		/// </summary>
		public Dictionary<string, string> attr { get; set; }
		public string state { get; set; }
		public List<jsTreeNode> children { get; set; }

		/// <summary>
		/// This can be a string or an object of any serializable type. It will be stored in the data attribute of an html element, json encoded
		/// </summary>
		public object metadata { get; set; } 
		public string language { get; set; }
	}
}