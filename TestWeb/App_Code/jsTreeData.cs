﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.App_Code
{
	[Serializable]
	public class jsTreeData
	{
		public string title { get; set; }
		public Dictionary<string, string> attr { get; set; }
		public string icon { get; set; }
	}
}