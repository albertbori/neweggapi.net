﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using NeweggAPI;
using NeweggAPI.Query;

namespace UnitTests
{
	[TestClass]
	public class Query
	{
		[TestMethod]
		public void TestSearchAllProducts()
		{
			SearchResult result = NeweggAPI.API.GetSearchResult(0);

			Assert.IsNotNull(result);

			Assert.AreEqual(0, result.PaginationInfo.PageNumber);
			Assert.IsTrue(result.PaginationInfo.PageSize > 0);
			Assert.IsTrue(result.PaginationInfo.TotalCount > 0);

			Assert.IsTrue(result.ProductListItems.Count > 0);

			Assert.IsNotNull(result.ProductListItems[0]);
			Assert.IsNotNull(result.ProductListItems[0].Title);
		}

		[TestMethod]
		public void TestTextSearch()
		{
			SearchParams query = new SearchParams();
			query.Keyword = "Power Supply";

			SearchResult result = NeweggAPI.API.GetSearchResult(query);

			Assert.IsNotNull(result);
			Assert.IsNotNull(result.PaginationInfo);
			Assert.IsNotNull(result.ProductListItems);
			Assert.IsTrue(result.ProductListItems.Count > 0, "No products were found.");
			//Assert.IsNotNull(result.NavigationContentList);
			//Assert.IsTrue(result.NavigationContentList.Count > 0);
			//Assert.IsNotNull(result.NavigationContentList[0]);
		}
	}
}
