﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
	[TestClass]
	public class Deals
	{
		[TestMethod]
		public void TestMainDeal()
		{
			NeweggAPI.Deals d = NeweggAPI.API.GetMainDeals();

			Assert.IsNotNull(d);
		}

		[TestMethod]
		public void TestShellShocker()
		{
			NeweggAPI.Deals d = NeweggAPI.API.GetMainDeals();

			Assert.IsNotNull(d.ShellShocker);
			Assert.IsNotNull(d.ShellShocker.ShellShockerItemNumber);
			Assert.IsTrue(d.ShellShocker.ProductList.Count > 0);
			Assert.IsNotNull(d.ShellShocker.ProductList[0].Title);
		}

		[TestMethod]
		public void TestDailyDeals()
		{
			NeweggAPI.Deals d = NeweggAPI.API.GetMainDeals();

			Assert.IsNotNull(d.DailyDeals);
			Assert.IsTrue(d.DailyDeals.Count > 0);
			Assert.IsNotNull(d.DailyDeals[0].ItemNumber);
		}
	}
}
