﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeweggAPI.Products;

namespace UnitTests
{
	[TestClass]
	public class Products
	{
		[TestMethod]
		public void TestGetProduct()
		{
			ProductDetails product = NeweggAPI.API.GetProductDetails("35-221-015");

			Assert.IsNotNull(product.AddToCartButtonText);
			Assert.IsNotNull(product.AddToCartButtonType);
			Assert.IsNotNull(product.AverageRating);
			Assert.IsNotNull(product.BrandInfo);
			Assert.IsNotNull(product.CanAddToCart);
			Assert.IsNotNull(product.ComboCount);
			Assert.IsNotNull(product.CoremetricsInfo);
			Assert.IsNull(product.Discount);
			Assert.IsNotNull(product.EmailFriendImageInfo);
			Assert.IsNull(product.EnergyStarText);
			Assert.IsNotNull(product.ETA);
			Assert.IsNotNull(product.FinalPrice);
			Assert.IsNotNull(product.FreeShippingFlag);
			Assert.IsNotNull(product.HasMappingPrice);
			Assert.IsNotNull(product.HasSimilarURL);
			Assert.IsNotNull(product.Image);
			Assert.IsNotNull(product.imageGallery);
			Assert.IsNotNull(product.InstantSaving);
			Assert.IsNotNull(product.Instock);
			Assert.IsNotNull(product.InstockForCombo);
			Assert.IsNull(product.IronEggDescription);
			Assert.IsNotNull(product.IsActivated);
			Assert.IsNotNull(product.IsCellPhoneItem);
			Assert.IsNotNull(product.IsComboBundle);
			Assert.IsNotNull(product.IsFeaturedItem);
			Assert.IsNotNull(product.IsHot);
			Assert.IsNotNull(product.IsInPMCC);
			Assert.IsNotNull(product.IsPreLaunch);
			Assert.IsNotNull(product.IsShellShockerItem);
			Assert.IsNotNull(product.IsShipByNewegg);
			Assert.IsNotNull(product.IsShowEnergyStarSection);
			Assert.IsNotNull(product.IsShowSoldOutText);
			Assert.IsNotNull(product.ItemGroupID);
			Assert.IsNotNull(product.ItemMapPriceMarkType);
			Assert.IsNotNull(product.ItemNumber);
			Assert.IsNotNull(product.ItemOwnerType);
			Assert.IsNotNull(product.ItemType);
			Assert.IsNotNull(product.LimitQuantity);
			Assert.IsNull(product.MailInRebateInfo);
			Assert.IsNull(product.MailInRebateText);
			Assert.IsNull(product.MappingFinalPrice);
			Assert.IsNotNull(product.Model);
			Assert.AreEqual("KDE1204PKVX", product.Model);
			Assert.IsNull(product.NeweggItemNumber);
			Assert.IsNotNull(product.NumberOfReviews);
			Assert.IsNotNull(product.NValue);
			Assert.IsNotNull(product.OriginalPrice);
			Assert.IsNull(product.ParentItem);
			Assert.IsNotNull(product.ProductStockType);
			Assert.IsNull(product.PromotionInfo);
			Assert.IsNotNull(product.PromotionText);
			Assert.IsNotNull(product.ReturnPolicyInfo);
			Assert.IsNotNull(product.ReviewSummary);
			Assert.IsNull(product.SellerId);
			Assert.IsNull(product.SellerItemPropertyList);
			Assert.IsNull(product.SellerName);
			Assert.IsNotNull(product.ShipByNewegg);
			Assert.IsNotNull(product.ShippingInfo);
			Assert.IsNull(product.ShippingPromotionInfo);
			Assert.IsNotNull(product.ShowOriginalPrice);
			Assert.IsNull(product.StaticText);
			Assert.IsNull(product.StrAddItem);
			Assert.IsNull(product.StrAlt);
			Assert.IsNull(product.StrCartImg);
			Assert.IsNotNull(product.SubCategoryId);
			Assert.IsNotNull(product.SubCategoryName);
			Assert.IsNotNull(product.Title);
			Assert.AreEqual("SUNON KDE1204PKVX 40mm Case Fan", product.Title);
			Assert.IsNull(product.uiExtendedWarrantyContent);
			Assert.IsNotNull(product.uiVolumeDiscountInfo);
			Assert.IsNotNull(product.UnitPrice);
			Assert.IsNull(product.Warnings);
			Assert.IsNull(product.WarrantyInfo);
			Assert.IsNull(product.XmlSpec);
		}
	}
}
