﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;

namespace UnitTests
{
	[TestClass]
	public class Stores
	{
		[TestMethod]
		public void TestGetAllStores()
		{
			ReadOnlyCollection<NeweggAPI.Store> stores = NeweggAPI.API.GetAllStores();

			Assert.IsTrue(stores.Count > 0, "Error retrieving store data from newegg api.");
		}

		[TestMethod]
		public void TestGetCategoriesProperty()
		{
			ReadOnlyCollection<NeweggAPI.Store> stores = NeweggAPI.API.GetAllStores();

			Assert.IsTrue(stores[0].Categories.Count > 0, "Error retrieving store data from newegg api.");
		}

		[TestMethod]
		public void TestNeweggCategories()
		{
			ReadOnlyCollection<NeweggAPI.Category> categories = NeweggAPI.API.GetMainCategories(1);

			Assert.IsTrue(categories.Count > 0, "Error retrieving category data from newegg api.");
		}

		[TestMethod]
		public void TestGetSubCategoriesProperty()
		{
			ReadOnlyCollection<NeweggAPI.Category> categories = NeweggAPI.API.GetMainCategories(1);

			Assert.IsNotNull(categories[0].SubCategories);
			Assert.IsTrue(categories[0].SubCategories.Count > 0, "Error sub-category store data from newegg api.");
		}
	}
}
