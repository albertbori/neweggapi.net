﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;

namespace UnitTests
{
	[TestClass]
	public class Specs
	{
		[TestMethod]
		public void TestSpecSerialization()
		{
			NeweggAPI.Products.ProductSpecifications specs = NeweggAPI.API.GetProductSpecifications("N82E16813128519");

			Assert.IsTrue(specs.SpecificationGroupList.Count > 0);
			Assert.AreEqual("Model", specs.SpecificationGroupList[0].GroupName);
			Assert.IsTrue(specs.SpecificationGroupList[0].SpecificationPairList.Count > 0);

			Assert.AreEqual("Brand", specs.SpecificationGroupList[0].SpecificationPairList[0].Key);
			Assert.AreEqual("GIGABYTE", specs.SpecificationGroupList[0].SpecificationPairList[0].Value);
		}
	}
}
