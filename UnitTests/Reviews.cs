﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeweggAPI.Reviews;

namespace UnitTests
{
	[TestClass]
	public class Reviews
	{
		[TestMethod]
		public void TestProductReviews()
		{
			ProductReviews reviews = NeweggAPI.API.GetProductReviews("35-221-015");

			Assert.IsNotNull(reviews);

			Assert.IsNotNull(reviews.IpadReviews);
			Assert.AreEqual(3, reviews.IpadReviews.Count);
			Assert.IsNotNull(reviews.IpadReviews[0].BoughtTimeTypeInt);
			Assert.IsNotNull(reviews.IpadReviews[0].BoughtTimeTypeString);
			Assert.IsNotNull(reviews.IpadReviews[0].Comments);
			Assert.IsNotNull(reviews.IpadReviews[0].Cons);
			Assert.IsNotNull(reviews.IpadReviews[0].IsNewReview);
			Assert.IsNotNull(reviews.IpadReviews[0].LoginNickName);
			Assert.IsNotNull(reviews.IpadReviews[0].Pros);
			Assert.IsNotNull(reviews.IpadReviews[0].PublishDate);
			Assert.IsNotNull(reviews.IpadReviews[0].PurchaseMark);
			Assert.IsNotNull(reviews.IpadReviews[0].Rating);
			Assert.IsNotNull(reviews.IpadReviews[0].reviewID);
			Assert.IsNotNull(reviews.IpadReviews[0].TechLevelTypeInt);
			Assert.IsNotNull(reviews.IpadReviews[0].TechLevelTypeString);
			Assert.IsNotNull(reviews.IpadReviews[0].Title);
			Assert.IsNotNull(reviews.IpadReviews[0].TotalConsented);
			Assert.IsNotNull(reviews.IpadReviews[0].TotalVoting);

			Assert.IsNotNull(reviews.PaginationInfo.PageCount);
			Assert.IsNotNull(reviews.PaginationInfo.PageNumber);
			Assert.IsNotNull(reviews.PaginationInfo.PageSize);
			Assert.IsNotNull(reviews.PaginationInfo.TotalCount);

			Assert.IsNotNull(reviews.ProductImageInfo.FullPath);
			Assert.IsNull(reviews.ProductImageInfo.SmallImagePath);
			Assert.IsNull(reviews.ProductImageInfo.ThumbnailImagePath);
			Assert.IsNull(reviews.ProductImageInfo.Title);

			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating1Count);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating1Percent);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating2Count);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating2Percent);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating3Count);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating3Percent);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating4Count);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating4Percent);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating5Count);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.Rating5Percent);
			Assert.IsNotNull(reviews.ProductReviewBarInfo.RatingCounts);

			Assert.IsNull(reviews.Reviews);

			Assert.IsNotNull(reviews.ReviewSummaryInfo.AllReviewCount);
			Assert.IsNull(reviews.ReviewSummaryInfo.AllReviewDesc);
			Assert.IsNotNull(reviews.ReviewSummaryInfo.Last2WeeksCount);
			Assert.IsNull(reviews.ReviewSummaryInfo.Last2WeeksDesc);
			Assert.IsNotNull(reviews.ReviewSummaryInfo.Last6MonthsCount);
			Assert.IsNull(reviews.ReviewSummaryInfo.Last6MonthsDesc);

			Assert.IsNotNull(reviews.Summary);
		}
	}
}
