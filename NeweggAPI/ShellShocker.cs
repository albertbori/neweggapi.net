﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeweggAPI.Products;

namespace NeweggAPI
{
	[Serializable]
	public class ShellShocker
	{
		public bool IsSoldout { get; set; }
		public string OriginalPrice { get; set; }
		public string FinalPrice { get; set; }
		public string ShellShockerItemNumber { get; set; }
		public int ShellShockerType { get; set; }
		public List<ProductDetails> ProductList { get; set; }
		public bool DisplayPercent { get; set; }
		public string Discount { get; set; }
		public string Promotion { get; set; }
		public string PromotionInfo { get; set; }
		public bool IsFreeShipping { get; set; }
		public string ProductTitle { get; set; }
	}
}
