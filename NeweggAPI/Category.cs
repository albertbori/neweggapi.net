﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace NeweggAPI
{
	[Serializable]
	public class Category
	{
		public string Description { get; set; }
		public int StoreID { get; set; }
		public int NodeId { get; set; }
		public bool ShowSeeAllDeals { get; set; }
		public int CategoryType { get; set; }
		public int CategoryID { get; set; }

		private ReadOnlyCollection<Category> _subCategories;
		/// <summary>
		/// Lazily retrieves the sub-categories that belong to this category.
		/// </summary>
		public ReadOnlyCollection<Category> SubCategories
		{
			get
			{
				if (_subCategories == null)
				{
					_subCategories = API.GetSubCategories(this);
				}

				return _subCategories;
			}
		}
	}
}
