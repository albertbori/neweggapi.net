﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace NeweggAPI
{
	[Serializable]
	public class Store
	{
		public string StoreDepa { get; set; }
		public int StoreID { get; set; }
		public bool ShowSeeAllDeals { get; set; }
		public string Title { get; set; }

		[NonSerialized]
		private ReadOnlyCollection<Category> _categories;
		/// <summary>
		/// Lazily retrieves the categories that belong to this store.
		/// </summary>
		public ReadOnlyCollection<Category> Categories
		{
			get
			{
				if (_categories == null)
				{
					_categories = API.GetMainCategories(this.StoreID);
				}

				return _categories;
			}
		}
	}
}
