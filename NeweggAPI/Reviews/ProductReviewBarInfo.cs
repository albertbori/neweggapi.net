﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Reviews
{
	[Serializable]
	public class ProductReviewBarInfo
	{
		public int RatingCounts { get; set; }
		public string Rating1Count { get; set; }
		public int Rating1Percent { get; set; }
		public string Rating2Count { get; set; }
		public int Rating2Percent { get; set; }
		public string Rating3Count { get; set; }
		public int Rating3Percent { get; set; }
		public string Rating4Count { get; set; }
		public int Rating4Percent { get; set; }
		public string Rating5Count { get; set; }
		public int Rating5Percent { get; set; }
	}
}
