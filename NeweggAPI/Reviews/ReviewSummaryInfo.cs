﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Reviews
{
	[Serializable]
	public class ReviewSummaryInfo
	{
		public int Last2WeeksCount { get; set; }
		public string Last2WeeksDesc { get; set; } // ?
		public int Last6MonthsCount { get; set; }
		public string Last6MonthsDesc { get; set; } // ?
		public int AllReviewCount { get; set; }
		public string AllReviewDesc { get; set; } // ?
	}
}
