﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NeweggAPI.Products;
using NeweggAPI.Query;

namespace NeweggAPI.Reviews
{
	[Serializable]
	public class ProductReviews
	{
		public List<Review> IpadReviews { get; set; }
		public ReviewSummaryInfo ReviewSummaryInfo { get; set; }
		public ReviewSummary Summary { get; set; }
		public object Reviews { get; set; } // ? this was null...
		public PaginationInfo PaginationInfo { get; set; }
		public ProductImage ProductImageInfo { get; set; }
		public ProductReviewBarInfo ProductReviewBarInfo { get; set; }
	}
}
