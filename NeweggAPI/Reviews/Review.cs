﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Reviews
{
	[Serializable]
	public class Review
	{
		public int BoughtTimeTypeInt { get; set; }
		public int TechLevelTypeInt { get; set; }
		public int reviewID { get; set; }
		public string Title { get; set; }
		public int Rating { get; set; }
		public string PublishDate { get; set; }
		public string LoginNickName { get; set; }
		public string BoughtTimeTypeString { get; set; }
		public string TechLevelTypeString { get; set; }
		public bool IsNewReview { get; set; }
		public bool PurchaseMark { get; set; }
		public string Cons { get; set; }
		public string Pros { get; set; }
		public string Comments { get; set; }
		public int TotalConsented { get; set; }
		public int TotalVoting { get; set; }
	}
}
