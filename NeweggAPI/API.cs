﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.ObjectModel;

using NeweggAPI.Query;
using NeweggAPI.Products;
using NeweggAPI.Reviews;

namespace NeweggAPI
{
	public static class API
	{
		private static Dictionary<string, string> _apiCache = new Dictionary<string, string>();
		private static DateTime LastCacheClear;

		public static string NeweggAPIDomain = "http://www.ows.newegg.com";


		public static Deals GetMainDeals()
		{
			string json = GetJsonFromUrl(NeweggAPIDomain);

			Deals deals = DeserializeJson<Deals>(json);

			return deals;
		}

		public static ReadOnlyCollection<Store> GetAllStores()
		{
			string json = GetJsonFromUrl(NeweggAPIDomain + "/Stores.egg/Menus");

			ReadOnlyCollection<Store> stores = DeserializeJson<List<Store>>(json).AsReadOnly();

			return stores;
		}

		public static ReadOnlyCollection<Category> GetMainCategories(int storeID)
		{
			string json = GetJsonFromUrl(String.Format("{0}/Stores.egg/Categories/{1}", NeweggAPIDomain, storeID.ToString()));

			ReadOnlyCollection<Category> categories = DeserializeJson<List<Category>>(json).AsReadOnly();

			return categories;
		}

		public static ReadOnlyCollection<Category> GetSubCategories(Category parent)
		{
			return GetSubCategories(parent.StoreID, parent.CategoryID, parent.NodeId);
		}

		public static ReadOnlyCollection<Category> GetSubCategories(int storeID, int parentCategoryID, int nodeID)
		{
			string json = GetJsonFromUrl(String.Format("{0}/Stores.egg/Navigation/{1}/{2}/{3}", NeweggAPIDomain, storeID, parentCategoryID, nodeID));

			ReadOnlyCollection<Category> categories = DeserializeJson<List<Category>>(json).AsReadOnly();

			return categories;
		}

		/// <summary>
		/// Gets an object with all items from newegg.
		/// </summary>
		/// <param name="pageNumber">Desired page number of results. (0 based)</param>
		/// <returns></returns>
		public static SearchResult GetSearchResult(int pageNumber)
		{
			SearchParams queryParams = new SearchParams();
			queryParams.PageNumber = pageNumber;

			return GetSearchResult(queryParams);
		}

		public static SearchResult GetSearchResult(SearchParams queryParams)
		{
			string json = GetJsonFromUrl(NeweggAPIDomain + "/Search.egg/Advanced", SerializeJson(queryParams));

			SearchResult result = DeserializeJson<SearchResult>(json);

			return result;
		}

		public static ProductDetails GetProductDetails(string itemID)
		{
			string json = GetJsonFromUrl(String.Format("{0}/Products.egg/{1}/ProductDetails", NeweggAPIDomain, itemID));

			ProductDetails itemInfo = DeserializeJson<ProductDetails>(json);

			return itemInfo;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="itemID"></param>
		/// <param name="pageNumber"></param>
		/// <param name="filterTime">Not sure what this is. It was "all", with no way to select a date range in the app. Parameter defauls to "all".</param>
		/// <param name="filterRating">Use ReviewRatingFilters struct.</param>
		/// <param name="sort">Use ReviewRatingSorts struct</param>
		/// <returns></returns>
		public static ProductReviews GetProductReviews(string itemID, int pageNumber = 1, string filterTime = "all", string filterRating = "All", string sort = "")
		{
			string json = GetJsonFromUrl(String.Format("{0}/Products.egg/{1}/Reviewsinfo/{2}?filter.time={3}&filter.rating={4}&sort={5}",
				NeweggAPIDomain, itemID, pageNumber, filterTime, filterRating, sort));

			ProductReviews itemInfo = DeserializeJson<ProductReviews>(json);

			return itemInfo;
		}

		public static ProductSpecifications GetProductSpecifications(string itemID)
		{
			string json = GetJsonFromUrl(String.Format("{0}/Products.egg/{1}/Specification",
				NeweggAPIDomain, itemID));

			ProductSpecifications specification = DeserializeJson<ProductSpecifications>(json);

			return specification;
		}

		/// <summary>
		/// This method builds a newegg checkout link with a list of item numbers OR ID's
		/// </summary>
		/// <param name="productsIDsToBuy">list of item numbers OR ID's</param>
		/// <returns></returns>
		public static string BuildCheckoutLink(List<string> productsIDsToBuy)
		{
			//http://secure.newegg.com/Shopping/AddToCart.aspx?Submit=ADD&ItemList=20-231-476%7c1%2c17-371-016%7c1%2c19-115-074%7c1%2c13-131-707%7c1%2c14-102-908%7c1%2c20-233-206%7c1%2c11-129-042%7c1
			string checkoutURL = "http://secure.newegg.com/Shopping/AddToCart.aspx?Submit=ADD&ItemList={0}";
			string delimiter = "%7c1%2c";

			StringBuilder sb = new StringBuilder();
			foreach (String productID in productsIDsToBuy)
			{
				if(sb.Length > 0)
					sb.Append(delimiter);

				sb.Append(productID);
			}

			return string.Format(checkoutURL, sb.ToString());
		}

		#region Helpers
		private static T DeserializeJson<T>(string json)
		{
			return new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<T>(json);
		}

		private static string SerializeJson(object o)
		{
			return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(o);
		}

		private static string GetJsonFromUrl(string URL)
		{
			return GetJsonFromUrl(URL, null);
		}

		private static string GetJsonFromUrl(string URL, string postJson)
		{
			//Handle self-clearing api cache. It shouldn't last longer than 23 hours (fresh every day)
			if (LastCacheClear == null)
				LastCacheClear = DateTime.Now;
			else if (LastCacheClear.AddHours(23) < DateTime.Now)
			{
				LastCacheClear = DateTime.Now;
				_apiCache.Clear();
			}

			string cacheKey = URL + postJson;
			if (_apiCache.ContainsKey(cacheKey))
				return _apiCache[cacheKey];

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL);
			request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";

			if (!String.IsNullOrEmpty(postJson))
			{
				byte[] buffer = Encoding.ASCII.GetBytes(postJson);
				request.ContentType = "application/jsonrequest";
				request.Method = "POST";
				request.ContentLength = buffer.Length;

				Stream requestStream = request.GetRequestStream();
				requestStream.Write(buffer, 0, buffer.Length);
				requestStream.Close();
			}

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			if (response.StatusCode == HttpStatusCode.OK)
			{
				StreamReader sr = new StreamReader(response.GetResponseStream());
				string text = sr.ReadToEnd();
				_apiCache.Add(cacheKey, text);
				return text;
			}

			_apiCache.Add(cacheKey, String.Empty);
			return String.Empty;
		}

		public struct ReviewRatingFilters
		{
			public static string ALL = "All";
			public static string ONE_EGG = "1";
			public static string TWO_EGGS = "2";
			public static string THREE_EGGS = "3";
			public static string FOUR_EGGS = "4";
			public static string FIVE_EGGS = "5";
		}

		public struct ReviewRatingSorts
		{
			public static string NONE = String.Empty;
			public static string DATE_POSTED = "date%20posted";
			public static string MOST_HELPFUL = "most%20helpful";
			public static string HIGHEST_RATED = "highest%20rated";
			public static string LOWEST_RATED = "lowest%20rated";
			public static string OWNERSHIP = "ownership";
		}
		#endregion
	}
}
