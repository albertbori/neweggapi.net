﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using NeweggAPI.Products;

namespace NeweggAPI.Query
{
	[Serializable]
	public class SearchResult
	{
		public List<ResultsNavigation> NavigationContentList { get; set; }
		public PaginationInfo PaginationInfo { get; set; }
		public List<ProductDetails> ProductListItems { get; set; }

		public object RelatedLinkList { get; set; } //TODO: I don't know what type of object this is
		public bool IsAllComboBundle { get; set; }
		public bool CanBeCompare { get; set; }
		public int MasterComboStoreId { get; set; }
		public int SubCategoryId { get; set; }
		public bool HasDeactivatedItems { get; set; }
		public bool HasHasSimilarItems { get; set; }
		public int SearchProvider { get; set; }
		public int SearchResultType { get; set; }		
	}
}
