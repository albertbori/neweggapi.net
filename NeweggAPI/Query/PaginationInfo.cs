﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Query
{
	[Serializable]
	public class PaginationInfo
	{
		public int TotalCount { get; set; }
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public int PageCount { get; set; }
	}
}
