﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Query
{
	[Serializable]
	public class SearchFilter
	{
		public int SubCategoryId { get; set; }
		public string Description { get; set; }
		public int StoreDepaId { get; set; }
		/// <summary>
		/// This is a numeric value representing one of the Filters. Think of it as Search Filter ID.
		/// </summary>
		public string NValue { get; set; }
		public int BrandId { get; set; }
		public int StoreType { get; set; }
		public int ItemCount { get; set; }
		public int CategoryId { get; set; }
		public int ElementValue { get; set; }
	}
}
