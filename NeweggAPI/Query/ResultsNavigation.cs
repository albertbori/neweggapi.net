﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace NeweggAPI.Query
{
	[Serializable]
	public class ResultsNavigation
	{
		public List<SearchFilter> NavigationItemList { get; set; }
		public SearchFilter TitleItem { get; set; }
	}
}
