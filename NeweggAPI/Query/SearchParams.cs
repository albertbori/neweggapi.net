﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Query
{
	[Serializable]
	public class SearchParams
	{
		public int SubCategoryId { get; set; }
		/// <summary>
		/// This is a numeric value representing one of the Filters. Think of it as Search Filter ID.
		/// </summary>
		public string NValue { get; set; }
		public int StoreDepaId { get; set; }
		public int NodeId { get; set; }
		public int BrandId { get; set; }
		public int PageNumber { get; set; }
		public int CategoryId { get; set; }
		public bool IsUPCCodeSearch { get; set; }
		public bool IsSubCategorySearch { get; set; }
		public bool IsGuideAdvanceSearch { get; set; }
		/// <summary>
		/// Search query text. Example, "DDR2 4GB Ram"
		/// </summary>
		public string Keyword { get; set; }
		/// <summary>
		/// Use one of the static values in SortOptions struct within this class.
		/// </summary>
		public string Sort { get; set; }

		public struct SortOptions
		{
			/// <summary>
			/// For keyword searches. Returns best matches first
			/// </summary>
			public static string FEATURED = "FEATURED";
			public static string BEST_MATCH = "BESTMATCH";
			public static string BEST_RATING = "RATING";
			public static string LOWEST_PRICE = "PRICE";
			public static string HIGHEST_PRICE = "PRICED";
			public static string MOST_REVIEWS = "REVIEWS";
		}
	}
}
