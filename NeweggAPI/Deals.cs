﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI
{
	[Serializable]
	public class Deals
	{
		public ShellShocker ShellShocker { get; set; }
		public List<DailyDeal> DailyDeals { get; set; }
	}
}
