﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ProductImage
	{
		public string FullPath { get; set; }
		public string SmallImagePath { get; set; }
		public string ThumbnailImagePath { get; set; }
		public string Title { get; set; }
	}
}
