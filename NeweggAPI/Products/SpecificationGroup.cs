﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class SpecificationGroup
	{
		public SpecificationGroup() { }

		public string GroupName { get; set; }
		public List<Specification> SpecificationPairList { get; set; }
	}
}
