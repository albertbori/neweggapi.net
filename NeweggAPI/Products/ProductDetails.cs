﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ProductDetails
	{
		/* properties marked with a '?' were undeterminable at the time of parsing. They returned null. Could be a string or an object. */

		public List<VolumeDiscountInfo> uiVolumeDiscountInfo { get; set; }
		public object uiExtendedWarrantyContent { get; set; } //?
		public bool HasSimilarURL { get; set; }
		/// <summary>
		/// This is typically something like "similar[product-number]"
		/// </summary>
		public string NValue { get; set; }
		public ItemType ItemType { get; set; }
		public ReturnPolicyInfo ReturnPolicyInfo { get; set; }
		public object MailInRebateInfo { get; set; } //?
		public List<ProductImage> imageGallery { get; set; }
		public ShippingInfo ShippingInfo { get; set; }
		public bool IsShowSoldOutText { get; set; }
		public string AddToCartButtonText { get; set; }
		public string AddToCartButtonType { get; set; }
		public CoreMetricsInfo CoremetricsInfo { get; set; }
		public ProductImage EmailFriendImageInfo { get; set; }
		public object ShippingPromotionInfo { get; set; } //?
		public string IsActivated { get; set; }
		public object SellerItemPropertyList { get; set; } //?
		public bool IsShowEnergyStarSection { get; set; }
		public string EnergyStarText { get; set; }
		public string IronEggDescription { get; set; }
		public int ComboCount { get; set; }
		public int SubCategoryId { get; set; }
		public string SubCategoryName { get; set; }
		public object Warnings { get; set; } //?
		public bool IsShipByNewegg { get; set; }
		public bool IsCellPhoneItem { get; set; }
		public bool ShowOriginalPrice { get; set; }
		public string MailInRebateText { get; set; }
		public bool IsComboBundle { get; set; }
		public int ProductStockType { get; set; }
		public object MappingFinalPrice { get; set; } //?
		public bool IsFeaturedItem { get; set; }
		public string Title { get; set; }
		public string OriginalPrice { get; set; }
		public string Discount { get; set; }
		public string FinalPrice { get; set; }
		public bool FreeShippingFlag { get; set; }
		public string Model { get; set; }
		public ReviewSummary ReviewSummary { get; set; }
		public ProductImage Image { get; set; }
		public string SellerName { get; set; }
		public object ParentItem { get; set; } //?
		public object SellerId { get; set; } //?
		public int ShipByNewegg { get; set; }
		public int ItemGroupID { get; set; }
		public int ItemOwnerType { get; set; }
		public int NumberOfReviews { get; set; }
		public int AverageRating { get; set; }
		public bool Instock { get; set; }
		public object XmlSpec { get; set; } //?
		public object WarrantyInfo { get; set; } //?
		public string NeweggItemNumber { get; set; } //?
		public object PromotionInfo { get; set; } //?
		public bool InstockForCombo { get; set; }
		public DateTime ETA { get; set; }
		public bool IsInPMCC { get; set; }
		public bool CanAddToCart { get; set; }
		public string StrCartImg { get; set; }
		public string StrAlt { get; set; }
		public string StrAddItem { get; set; }
		public string StaticText { get; set; }
		public int InstantSaving { get; set; }
		public bool HasMappingPrice { get; set; }
		public int UnitPrice { get; set; }
		public BrandInfo BrandInfo { get; set; }
		public int LimitQuantity { get; set; }
		public bool IsShellShockerItem { get; set; }
		public string PromotionText { get; set; }
		public int ItemMapPriceMarkType { get; set; }
		public bool IsHot { get; set; }
		public bool IsPreLaunch { get; set; }
		/// <summary>
		/// This is the full newegg item ID that you see in the url when browsing their site.
		/// </summary>
		public string ItemNumber { get; set; }
	}
}
