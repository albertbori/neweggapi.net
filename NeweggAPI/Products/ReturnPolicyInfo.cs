﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ReturnPolicyInfo
	{
		public string Name { get; set; }
		public int ID { get; set; }
		public string HtmlContent { get; set; }
	}
}
