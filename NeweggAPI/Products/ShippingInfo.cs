﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ShippingInfo
	{
		public string NormalShippingText { get;set; }
		public string SpecialShippingText { get;set; }
		public string RestrictedShippingTitle { get;set; }
		public string RestrictedShippingText { get; set; }
	}
}
