﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	public class ProductSpecifications
	{
		public ProductSpecifications() { }

		public string NeweggItemNumber { get; set; }
		public string Title { get; set; }
		public List<SpecificationGroup> SpecificationGroupList { get; set; }
	}
}
