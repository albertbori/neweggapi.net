﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class BrandInfo
	{
		public int Code { get; set; }
		public int BrandId { get; set; }
		public string Description { get; set; }
		public string ManufactoryWeb { get; set; }
		public string WebSiteURL { get; set; }
		public bool HasManfactoryLogo { get; set; }
		public string BrandImage { get; set; }
	}
}
