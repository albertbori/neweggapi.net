﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	public class ReturnPolicy
	{
		public string Name { get; set; }
		public string ID { get; set; }
		public string HtmlContent { get; set; }
	}
}
