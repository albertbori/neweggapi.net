﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ItemType
	{
		public bool IsDigitalRiverItem { get; set; }
		public bool IsDVDItem { get; set; }
		public bool IsOpenBoxItem { get; set; }
		public bool IsRecertifiedItem { get; set; }
		public bool IsAITItem { get; set; }
		public bool IsPhoneItem { get; set; }
		public bool IsServicePlanItem { get; set; }
		public bool IsSimCardItem { get; set; }
		public bool IsRestrictedItem { get; set; }
		public bool IsComboBundle { get; set; }
		public bool IsSimplxityItem { get; set; }
	}
}
