﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class ReviewSummary
	{
		public double Rating { get; set; }
		public string TotalReviews { get; set; }
	}
}
