﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class Specification
	{
		public Specification() { }

		public string Key { get; set; }
		public string Value { get; set; }
	}
}
