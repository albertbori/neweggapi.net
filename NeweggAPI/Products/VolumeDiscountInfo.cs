﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI.Products
{
	[Serializable]
	public class VolumeDiscountInfo
	{
		public int MinQuantity { get; set; }
		public int MaxQuantity { get; set; }
		public string KeyQuantity { get; set; }
		public string Price { get; set; }
	}
}
