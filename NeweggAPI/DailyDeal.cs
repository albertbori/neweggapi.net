﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeweggAPI
{
	[Serializable]
	public class DailyDeal
	{
		public string ItemNumber { get; set; }
		public string Title { get; set; }
		public string FullPath { get; set; }
		public string ThumbnailImagePath { get; set; }
		public string SmallImagePath { get; set; }
	}
}
